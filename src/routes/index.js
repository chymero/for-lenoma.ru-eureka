/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [
  // Home
  {
    path: '/',
    component: () => import('@/views/landing.vue'),

    // If the user needs to be authenticated to view this page
  },
];
